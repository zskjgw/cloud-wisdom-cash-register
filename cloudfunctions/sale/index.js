// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command;
// 云函数入口函数
exports.main = async(event, context) => {
    try {
        if (event.currentTab != 3) {
            return await db.collection('pay_count').where({
                    date: _.gte(event.strat).and(_.lte(event.end)),
                    user_id:event.user_id,
                })
                .get()
        } else {
            return await db.collection('pay_count').where({
                    user_id:event.user_id,
                    date: _.lte(event.end).user_id,
                })
                .get()
        }
    } catch (e) {
        console.error(e)
    }
}