// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
const _ = db.command;
const $ = db.command.aggregate;

// 云函数入口函数
exports.main = async (event, context) => {
  try{
    if(event.kucun){
      return_msg = await db.collection('commodity').aggregate()
      .match({
        user_id:event.user_id,
      })
      .project({
        user_id:event.user_id,
        goods: $.filter({
          input: '$goods',
          as: 'item',
          cond: $.lte(['$$item.goods_num',3])
        })
      }).end();
    }
  }catch(e){
    return_msg = {"error":"1","msg":"未知异常:" + e}
  }
  return return_msg;
}