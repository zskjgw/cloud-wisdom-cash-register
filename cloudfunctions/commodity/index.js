// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
const _ = db.command;
const $ = db.command.aggregate;

var return_msg = [];
// 云函数入口函数
exports.main = async (event, wxContext) => {
  try{
      //新增商品
      if(event.add_goods){
        await db.collection('commodity').where({
          user_id: event.user_id,
        })
        .get().then(res => {
          console.log(res);
          if(res.data.length <= 0){
            //这里为判断是否该用户第一次新增商品
            console.log("无记录,执行新增");
            //无记录 新增记录
            db.collection('commodity').add({
              data: {
                user_id:event.user_id,
                openid:wxContext.OPENID,
                add_time:new Date().toLocaleString(),
                update_time:new Date().toLocaleString(),
                goods:[event.goods_list],
              }
            });
          }else{
            if(event.add){
              db.collection('commodity').where({
                user_id: event.user_id,
              }).update({
                data: {
                  goods: _.push(event.goods_list)
                }
              })
            }else{
              //存在商品 执行编辑操作
              db.collection('commodity').where({
                user_id: event.user_id,
                //根据商品条码进行编辑
                goods:{
                  bz:event.sptm2,
                }
              }).update({
                data: {
                  'goods.$': event.goods_list,
                }
              })
            }
          }
          return_msg = {"error":"0","msg":"操作成功"}
        })
      }
      //删除商品 根据商品条码删除指定商品
      if(event.del_goods){
        return await db.collection('commodity').where({
          user_id: event.user_id,
        }).update({
          data: {
            goods: _.pull({
              bz: _.eq(event.sptm)
          }),
          }
        })
      }
      //查询购物车商品
      if(event.search){
        await db.collection('commodity').where({
          user_id: event.user_id,
        })
        .skip(event.start_page).limit(event.end_page).get().then(res => {
          return_msg = res;
        })
      }
      //搜索指定商品 支持模糊匹配 搜寻条码和商品名称
      if(event.search_goods){
        return_msg = await db.collection('commodity').aggregate()
        .match({
          user_id:event.user_id,
        })
        .project({
          user_id:event.user_id,
          goods: $.filter({
            input: '$goods',
            as: 'item',
            cond: $.or(
              [$.gte([$.indexOfBytes(['$$item.goods_title',event.value]), 0]),
              $.gte([$.indexOfBytes(['$$item.bz',event.value]), 0])]
                      )
          })
        }).end();
      }
  }catch(e){
    return_msg = {"error":"1","msg":"未知异常:" + e}
  }
  return return_msg;
}