// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command;
const $ = db.command.aggregate;

// 云函数入口函数
exports.main = async (event, context) => {
  if(event.pd){
    return await db.collection('commodity').where({
      user_id: event.user_id,
      //根据商品条码进行编辑
      goods:{
        bz:event.sptm,
      }
    }).update({
      data: {
        'goods.$.goods_num':parseInt(event.pd_text),
      }
    })
  }
}