// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
var return_msg = [];

var http = require('http');
var iconv = require('iconv-lite');
var superagent = require("superagent");
var cheerio = require("cheerio");
var charset = require("superagent-charset");
charset(superagent); //设置字符

// 云函数入口函数
exports.main = async (event, context) => {
  return_msg = [];
  try{
    //支付宝支付
    if(event.pay_lx = 1){
      var url = "https://xxx.com/pays/alipay.php?code=" + event.code + "&money=" + event.money;
      await superagent.get(url)
      .then(res => {
        console.log(res);
        error = { "error": 0, "msg": "success" , "data": JSON.parse(res.text) }
        return_msg = error;
      }).catch(err => {
        console.log(err);
        error = { "error": 1, "msg": "支付异常" }
        return_msg = error;
      });
    //微信支付
    }else if(event.pay_lx = 2){
      var url = "https://xxx.com/pays/wechat.php?code=" + event.code + "&money=" + event.money;
      await superagent.get(url)
      .then(res => {
        console.log(res);
        error = { "error": 0, "msg": "success" , "data": JSON.parse(res.text) }
        return_msg = error;
      }).catch(err => {
        console.log(err);
        error = { "error": 1, "msg": "支付异常" }
        return_msg = error;
      });
    }else{
      return_msg = {"error":"1","msg":"未知异常"}
    }
  }catch(e){
    return_msg = {"error":"1","msg":"未知异常:" + e}
  }
  return return_msg;
}