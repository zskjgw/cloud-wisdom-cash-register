//app.js
App({
  onLaunch: function () {

    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
      })
    }
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule + e.statusBarHeight;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    })
  },
  gsh_money(e){
    e = parseFloat(e);
    e = e.toFixed(2);
    e = parseFloat(e);
    return e;
  },
  globalData: {
    user_id:null,
    pay_goods:[],
    address1:[],
    address2:'',
    shop:'',
    guest:0,
  }
})
