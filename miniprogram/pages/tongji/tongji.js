/**
 * 销售统计页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp();
var that, clientHeight, clientWidth;
var CHARTS = require('../../style/wxcharts-min.js');
function writepie(a,b,c,d) {
    new CHARTS({
      canvasId: 'columnCanvas',
      type: 'pie',
      series: [{ name: '现金', data: parseFloat(a) }, { name: '银行卡', data: parseFloat(b) }, { name: '微信', data: parseFloat(c) }, { name: '支付宝', data: parseFloat(d) }],
      width: clientWidth,
      height: 250,
      dataLabel: true,
      title: { name: '收款统计图' },
      animation: true,
      setdate:null,
      setfcip:0,
    });
  }
Page({
    data: {
        winHeight: "", //窗口高度
        currentTab: 0, //预设当前项的值
        scrollLeft: 0, //tab标题的滚动条位置
        chartTitle: '总订单',
        isMainChartDisplay: true,
        sale: []
    },
    // 滚动切换标签样式
    switchTab: function(e) {
        this.setData({
            currentTab: e.detail.current
        });
        this.checkCor();
    },
    // 点击标题切换当前页时改变样式
    swichNav: function(e) {
        var cur = e.target.dataset.current;
        console.log(cur);
        if (this.data.currentTaB == cur) { return false; } else {
            this.setData({
                currentTab: cur
            });
            // 获取订单信息
            this.sales();
        }
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor: function() {
        if (this.data.currentTab > 4) {
            this.setData({
                scrollLeft: 300
            })
        } else {
            this.setData({
                scrollLeft: 0
            })
        }
    },
    // 获取订单信息
    sales() {
        wx.showLoading({ title: '加载商品中', })
        this.setData({
            sale: []
        })
        let currentTab = this.data.currentTab
        var that = this;
        let date = new Date();
        // 年
        let years = date.getFullYear();
        //    月
        let month = date.getMonth() + 1;
        //    日
        let day = date.getDate();
        if (this.data.currentTab == 0) {
            day = day
        } else if (this.data.currentTab == 1) {
            day = day - 1
        }
        let strat = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 0 + ':' + 0 + ':' + 0).getTime() / 1000);
        let end = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 23 + ':' + 59 + ':' + 59).getTime() / 1000);
        if (this.data.currentTab == 2) {
            var monthStartDate = new Date(years, month-1, 1); 
            var monthEndDate = new Date(years, month, 0);
            strat=Date.parse(monthStartDate)/1000;
            end=Date.parse(monthEndDate)/1000;
        }
        if (this.data.currentTab == 3) {
            var monthStartDate = new Date(years, 1); 
            var monthEndDate = new Date(years+1, 0);
            strat=Date.parse(monthStartDate)/1000;
            end=Date.parse(monthEndDate)/1000;
        }
        var time = Math.round(new Date() / 1000);
        wx.cloud.callFunction({
            name: 'tongji',
            data: {
                tongji:true,
                user_id: app.globalData.user_id,
                currentTab: currentTab,
                strat: strat,
                end: end
            },
            complete: res => {
                wx.hideLoading();
                if(res.result.error == '0'){
                    //支付宝收款统计
                    if(res.result.alipay_count.list.length > 0){
                        that.setData({
                            alipay_count:app.gsh_money(res.result.alipay_count.list[0].sale_Price)
                        })
                    }
                    else{
                        that.setData({
                            alipay_count:'0.00'
                        })
                    }
                    //微信收款统计
                    if(res.result.wechat_count.list.length > 0){
                        that.setData({
                            wechat_count:app.gsh_money(res.result.wechat_count.list[0].sale_Price)
                        })
                    }
                    else{
                        that.setData({
                            wechat_count:'0.00'
                        })
                    }
                    //现金收款统计
                    if(res.result.money_count.list.length > 0){
                        that.setData({
                            money_count:app.gsh_money(res.result.money_count.list[0].sale_Price)
                        })
                    }
                    else{
                        that.setData({
                            money_count:'0.00'
                        })
                    }
                    //银行卡收款统计
                    if(res.result.card_count.list.length > 0){
                        that.setData({
                            card_count:app.gsh_money(res.result.card_count.list[0].sale_Price)
                        })
                    }
                    else{
                        that.setData({
                            card_count:'0.00'
                        })
                    }
                    //销售金额统计
                    if(res.result.price_count.list.length > 0){
                        that.setData({
                            price_count:app.gsh_money(res.result.price_count.list[0].sale_Price)
                        })
                    }
                    //销售数量统计
                    if(res.result.sale_count2.list.length > 0){
                        that.setData({
                            sale_count2:app.gsh_money(res.result.sale_count2.list[0].sale_num)
                        })
                    }
                    else{
                        that.setData({
                            sale_count2:'0'
                        })
                    }
                    that.setData({
                        sale_count:res.result.sale_count.total,
                    })
                    writepie(that.data.money_count,that.data.card_count,that.data.wechat_count,that.data.alipay_count);
                }else{
                    wx.showModal({
                      content: '加载数据失败,未知错误',
                      showCancel:false,
                    })
                }
            }
        })

    },
    onLoad: function() {
        // 获取订单信息
        this.sales();
        that = this;
        //  高度自适应
        wx.getSystemInfo({
            success: function(res) {
                clientHeight = res.windowHeight;
                clientWidth = res.windowWidth;
                var rpxR = 750 / clientWidth;
                var calc = clientHeight * rpxR - 180;
                // console.log(calc)
                that.setData({
                    winHeight: calc
                });
            }
        });
    },
    footerTap: app.footerTap
})