/**
 * 收银结算页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
let app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        return_money:'0.00',
        navH: app.globalData.CustomBar,
        // 点击支付图标
        listIndex:0,
        mode_list: [{
                title: '现金',
                url: '../../icons/wallet.png',
                backgroundImage: "linear-gradient(to bottom right, #f7cca8, #ec7a89)"
            },
            {
                title: '银行卡',
                url: '../../icons/credit_card.png',
                backgroundImage: "linear-gradient(to bottom right, #d5abf6, #9952cf)"
            },
            {
                title: '支付宝',
                url: '../../icons/alipay.png',
                backgroundImage: "linear-gradient(to bottom right, #a9d4fb, #4597f7)"
            },
            {
                title: '微信',
                url: '../../icons/wechat.png',
                backgroundImage: "linear-gradient(to bottom right, #9cf6b8, #63c77b)"
            }
        ]
    },
    modeList(e){
        this.setData({
            listIndex:e.currentTarget.dataset.list_index
        })
    },
    //获取购物车信息
    get_pay_card(e){
        if(app.globalData.pay_goods.goods != undefined){
            this.setData(app.globalData.pay_goods)
        }else{
            wx.showModal({
                content:'请先添加相关商品',
                showCancel:false,
                success:function(){
                    wx.navigateBack();
                }
            })
        }
    },
    //获取输入的现金数
    getprice(e){
        var value = e.detail.value;
        this.setData({
            sjsk:value,
        })
        //金额过大或过小限制
        if(parseFloat(value) > parseFloat(this.data.addPrice) && value.length <= 6){
            var return_money = parseFloat(parseFloat(value) - parseFloat(this.data.addPrice))
            this.setData({
                return_money:return_money,
            })
        }else{
            this.setData({
                return_money:'0.00',
            })
        }
    },
    //设置备注
    setbz(e){
        var value = e.detail.value;
        this.setData({
            bz:value,
        })
    },
    onLoad(){
        this.get_pay_card();
    },
    onShow(){
        this.setData({
            sjsk:this.data.addPrice,
        })
    },
    setdb(){
        wx.cloud.callFunction({
            name: 'pay_count',
            data: {
                user_id:app.globalData.user_id,
                Price:this.data.addPrice,
                goods:this.data.goods,
                price_num:this.data.price_num,
                return_money:this.data.return_money,
                bz:this.data.bz,
                pay_lx:this.data.listIndex,
                add_time:new Date().toLocaleString(),
                date:Math.round(new Date() / 1000),
            },
            complete: res => {
                console.log(res);
            }
        })
    },
    //支付宝支付
    alipay(){
        var that=this;
        wx.scanCode({
            success(res) {
                wx.showLoading({
                    title: '正在收款中',
                  })
                var value = res.result;
                wx.cloud.callFunction({
                    name: 'pay',
                    data: {
                        "pay_lx": "1",
                        "money":that.data.addPrice,
                        "code":value
                    },
                    complete: res => {
                        wx.hideLoading();
                        console.log(res);
                        if(res.result.data.error == '0'){
                            wx.showModal({
                                content: '支付成功,订单已完结!',
                                showCancel:false,
                                success:function(){
                                    that.setdb();
                                }
                            })
                        }else{
                            wx.showModal({
                              content: '支付失败,请重试!',
                              showCancel:false,
                            })
                        }
                    }
                })
            }
        });
    },
    //微信支付
    wechat(){
        var that=this;
        wx.scanCode({
            success(res) {
                wx.showLoading({
                    title: '正在收款中',
                  })
                var value = res.result;
                wx.cloud.callFunction({
                    name: 'pay',
                    data: {
                        "pay_lx": "2",
                        "money":that.data.addPrice,
                        "code":value
                    },
                    complete: res => {
                        wx.hideLoading();
                        if(res.result.data.error == '0'){
                            wx.showModal({
                                content: '支付成功,订单已完结!',
                                showCancel:false,
                                success:function(){
                                    that.setdb();
                                }
                            })
                        }else{
                            wx.showModal({
                              content: '支付失败,请重试!',
                              showCancel:false,
                            })
                        }
                    }
                })
            }
        });
    },
    //结算该订单
    gopay(){
        if(this.data.listIndex == '0'){
            this.setdb();
            wx.showModal({
              content: '订单结算成功!',
              showCancel:false,
              success:function(){
                  wx.reLaunch({
                    url: '/pages/index/index',
                  })
              }
            })
        }
        if(this.data.listIndex == '1'){
            this.setdb();
            wx.showModal({
                content: '订单结算成功!',
                showCancel:false,
                success:function(){
                    wx.reLaunch({
                      url: '/pages/index/index',
                    })
                }
            })
        }
        if(this.data.listIndex == '2'){
            this.alipay();
        }
        if(this.data.listIndex == '3'){
            this.wechat();
        }
    }
})