"use strict";

// pages/collections/collections.js4
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    navH: app.globalData.CustomBar,
    mode_list: [{
      title: '现金',
      url: '../../icons/wallet.png',
      backgroundImage: "linear-gradient(to bottom right, #f7cca8, #ec7a89)"
    }, {
      title: '银行卡',
      url: '../../icons/credit_card.png',
      backgroundImage: "linear-gradient(to bottom right, #d5abf6, #9952cf)"
    }, {
      title: '支付宝',
      url: '../../icons/alipay.png',
      backgroundImage: "linear-gradient(to bottom right, #a9d4fb, #4597f7)"
    }, {
      title: '微信',
      url: '../../icons/wechat.png',
      backgroundImage: "linear-gradient(to bottom right, #9cf6b8, #63c77b)"
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function onLoad(options) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function onShareAppMessage() {}
});