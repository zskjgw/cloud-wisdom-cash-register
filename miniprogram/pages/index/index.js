/**
 * 首页
 * 
 * 设计：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp()

Page({
    data:{
        sale_count:0,
        sale_money:'0.00',
    },
    onLoad: function (options) {
        var that = this
        setInterval(function () {
            that.time(that)
        }, 1000);
        this.get_sale();
    },
    // 时间设置
    time(e) {
        let date = new Date()
        // 年
        let years = date.getFullYear()
        //    月
        let month = date.getMonth() + 1
        month = month > 9 ? date.getMonth() : "0" + date.getMonth();
        //    日
        let day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
        //    时
        let hours = date.getHours() > 9 ? date.getHours() : "0" + date.getHours();
        //    分
        let minutes = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        //    秒
        let seconds = date.getSeconds() > 9 ? date.getSeconds() : "0" + date.getSeconds();
        e.setData({
            years,
            month,
            day,
            hours,
            minutes,
            seconds
        })

    },
    // 链接跳转
    tz(e) {
        console.log(e.currentTarget.dataset.index);
        var index = parseInt(e.currentTarget.dataset.index);
        var url = ''
        switch (index) {
            case 1:
                url = '../goods/goods';
                break;
            case 2:
                url = '../goodsManage/goodsManage';
                break;
            case 3:
                url = '../pay/pay';
                break;
            case 4:
                url = '../tongji/tongji';
                break;    
            case 5:
                url='../sale/sale'
                break;    
            case 6:
                url='../kucun/kucun'
                break;  
            case 7:
                url='../pd/pd'
                break;      
            case 9:
                url='../about/about'
                break;    
        }
        wx.navigateTo({
            url: url,
        });
    },
    get_sale(){
        var that=this;
        let date = new Date();
        let years = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let strat = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 0 + ':' + 0 + ':' + 0).getTime() / 1000);
        let end = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 23 + ':' + 59 + ':' + 59).getTime() / 1000);
        wx.showLoading({
          title: '加载销售数据中',
        })
        wx.cloud.callFunction({
            name: 'tongji',
            data: {
                tongji:true,
                user_id: app.globalData.user_id,
                currentTab: 0,
                strat: strat,
                end: end
            },
            complete: res => {
                wx.hideLoading();
                if(res.result.error == '0'){
                //销售金额统计
                    if(res.result.price_count.list.length > 0){
                        that.setData({
                            sale_money:app.gsh_money(res.result.price_count.list[0].sale_Price)
                        })
                    }
                    else{
                        that.setData({
                            sale_money:'0.00'
                        })
                    }
                    //销售数量统计
                    if(res.result.sale_count2.list.length > 0){
                        that.setData({
                            sale_count2:app.gsh_money(res.result.sale_count2.list[0].sale_num)
                        })
                    }
                    else{
                        that.setData({
                            sale_count2:'0'
                        })
                    }
                    that.setData({
                        sale_count:res.result.sale_count.total,
                    })
                }
            }
        })
    },
    exitlogin(){
        wx.showModal({
          content: '确定要切换登录账号?',
          success:function(res){
              if(res.confirm){
                wx.cloud.callFunction({
                    name: 'login',
                    data: {
                      exitlogin: true,
                    },
                    complete: res => {
                        wx.reLaunch({
                          url: '../sign/sign',
                        })
                    }
                })
              }
          }
        })
    },
})
