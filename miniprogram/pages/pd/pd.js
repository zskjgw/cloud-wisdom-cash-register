/**
 * 盘点页
 * 
 * 设计：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp();
var that;
Page({
  scode: function (e) {
    that.setData({
      search_text: e.detail.value,
    })
  },
  search_goods(code){
    wx.showLoading({
      title: '搜索商品中',
    })
    wx.cloud.callFunction({
      name: 'commodity',
      data: {
          search_goods: true,
          user_id: app.globalData.user_id,
          value: code,
      },
      complete: res => {
          wx.hideLoading();
          console.log(res);
          if (res.result.list.length > 0) {
            if (res.result.list[0].goods.length > 0) {
                that.setData({
                  goods:res.result.list[0].goods
                })
            }else{
              wx.showModal({
                content: '未找到对应商品',
                showCancel:false,
            })
            }
          } else {
              wx.showModal({
                  content: '未找到对应商品',
              })
          }
      }
  })
  },
  scancode: function () {
    var that=this;
    // 允许从相机和相册扫码
    wx.scanCode({
      success(res) {
        console.log(res);
        wx.showToast({
          title: '成功',
          icon: 'success',
          duration: 2000
        })
        //识别成功输出条码
        if (isNaN(res.result)) {
          wx.showToast({
            title: '条形码类型不正确',
            icon: 'none',
            duration: 2000
          })
        }
        else {
          that.setData({
            search_text: res.result,
          });
          that.search_goods(res.result);
        }
      },
      fail: (res) => {
        console.log(res);
        wx.showToast({
          title: '失败',
          icon: 'none',
          duration: 2000
        })
      }
    })
  },
  pd_text: function (e) {
    this.setData({
      pd_text: e.detail.value
    })
  },
  search: function () {
    var code = that.data.search_text;
    if (isNaN(code)) {
      if (code.length >= 2) {
        this.search_goods(that.data.search_text);
      }
      else {
        wx.showToast({
          title: '请输入2个以上汉字',
          icon: 'none',
          duration: 2000
        })
      }
    }
    else {
      if (code.length >= 4) {
        this.search_goods(that.data.search_text);
      }
      else {
        wx.showToast({
          title: '请输入4个以上数字',
          icon: 'none',
          duration: 2000
        })
      }
    }
  },
  tz:function(e){
    let query = e.currentTarget.dataset['index'];
    that.setData({
      sptm: query,
    })
    if (that.data.pd_text == null)
    {
      wx.showToast({
        title: '请输入商品数量',
        icon: 'none',
        duration: 2000
      })
    }
    else
    {
      this.pd();
    }
  },
  pd(){
    var that=this;
    wx.cloud.callFunction({
      name: 'pd',
      data: {
          pd: true,
          user_id: app.globalData.user_id,
          sptm: that.data.sptm,
          pd_text:parseInt(that.data.pd_text),
      },
      complete: res => {
        wx.showToast({
          title: '盘点成功',
          icon: 'none',
          duration: 2000
        })
        that.setData({
          search_text: null,
          goods: null,
          pd_text: null,
          sptm: null,
        })
      }
    })
  },
  onLoad: function () {
    that = this;
    //初始化数据
    this.setData({
      search_text: null,
      typeArray: null,
      pd_text:null,
      sptm:null,
    });
  },
})