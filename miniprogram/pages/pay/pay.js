// pages/pay/pay.js
/**
 * 支付页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        maxlength: 5,
        Chinese_pay: '',
        paynum: '',
        disabled_dian: true,
        disabled_pay: true,
        address1:[],
        address2:'',
        shop:'',
    },
    onLoad(){
        this.setData({
            address1:app.globalData.address1,
            address2:app.globalData.address2,
            shop:app.globalData.shop,
        })
    },
    Num(e) {
        let num = e.currentTarget.dataset.num;
        let numadd = this.data.paynum;
        if(numadd.length == 1 && parseFloat(numadd) == 0){
            if(num != '.' && num != 'del'){
                return false;
            }
        }
        // 删除
        if (num == "del") {
            num = '';
            numadd = numadd.substr(0, numadd.length - 1);
            this.payNum(numadd);
            this.setData({
                paynum: numadd,
            })
        }
        if (num == 'pay') {
            num = '';
            this.gopay();
        } else {
            numadd += num;
            if (numadd.indexOf('.') != -1) {
                let x = String(numadd).indexOf('.') + 1; //小数点的位置
                let y = String(numadd).length - x; //小数的位数
                if (y <= 2) {
                    this.payNum(numadd);
                    this.setData({
                        paynum: numadd,
                        disabled_dian: true
                    })
                }
            } else {
                if (numadd.length <= 5) {
                    this.payNum(numadd);
                    this.setData({
                        paynum: numadd,
                        disabled_dian: false
                    })
                }
            }
        }
        // 判断金额是否为空，控制付款按钮
        if(numadd.indexOf(".") != -1){
            if(numadd.split(".")[1] == ""){
                this.setData({
                    disabled_pay: true,
                })
            }
            else{
                if(numadd == '0.00' || parseFloat(numadd) == 0)
                {
                    this.setData({
                        disabled_pay: true,
                    })
                }else{
                    if (numadd.length != 0) {
                        this.setData({
                            disabled_pay: false,
                        })
                    } else {
                        this.setData({
                            disabled_dian: true,
                            disabled_pay: true,
                        })
                    }
                }
            }
        }else{
            if(numadd == '0.00' || parseFloat(numadd) == 0)
            {
                this.setData({
                    disabled_pay: true,
                })
            }else{
                if (numadd.length != 0) {
                    this.setData({
                        disabled_pay: false,
                    })
                } else {
                    this.setData({
                        disabled_dian: true,
                        disabled_pay: true,
                    })
                }
            }
        }
    },
    payNum(num) {
        let pd_num = parseFloat(num)
        let maxlength = num.indexOf('.');
        var fuhao = "";
        var text = num + "";
        if (text.indexOf("-") > -1) {
            num = text.replace("-", "");
            fuhao = "负"
        }
        var money1 = new Number(num);
        var monee = Math.round(money1 * 100).toString(10);
        var leng = monee.length;
        var monval = "";
        for (let i = 0; i < leng; i++) {
            monval = monval + this.to_upper(monee.charAt(i)) + this.to_mon(leng - i - 1)
        }
        this.setData({
            Chinese_pay: fuhao + this.repace_acc(monval)
        })
    },
    to_upper(a) {
        switch (a) {
            case "0":
                return "零";
                break;
            case "1":
                return "壹";
                break;
            case "2":
                return "贰";
                break;
            case "3":
                return "叁";
                break;
            case "4":
                return "肆";
                break;
            case "5":
                return "伍";
                break;
            case "6":
                return "陆";
                break;
            case "7":
                return "柒";
                break;
            case "8":
                return "捌";
                break;
            case "9":
                return "玖";
                break;
            default:
                return ""
        }
    },
    to_mon(a) {
        if (a > 10) {
            a = a - 8;
            return (to_mon(a))
        }
        switch (a) {
            case 0:
                return "分";
                break;
            case 1:
                return "角";
                break;
            case 2:
                return "元";
                break;
            case 3:
                return "拾";
                break;
            case 4:
                return "佰";
                break;
            case 5:
                return "仟";
                break;
            case 6:
                return "万";
                break;
            case 7:
                return "拾";
                break;
            case 8:
                return "佰";
                break;
            case 9:
                return "仟";
                break;
            case 10:
                return "亿";
                break
        }
    },
    repace_acc(Money) {
        Money = Money.replace("零分", "");
        Money = Money.replace("零角", "零");
        var yy;
        var outmoney;
        outmoney = Money;
        yy = 0;
        while (true) {
            var lett = outmoney.length;
            outmoney = outmoney.replace("零元", "元");
            outmoney = outmoney.replace("零万", "万");
            outmoney = outmoney.replace("零亿", "亿");
            outmoney = outmoney.replace("零仟", "零");
            outmoney = outmoney.replace("零佰", "零");
            outmoney = outmoney.replace("零零", "零");
            outmoney = outmoney.replace("零拾", "零");
            outmoney = outmoney.replace("亿万", "亿零");
            outmoney = outmoney.replace("万仟", "万零");
            outmoney = outmoney.replace("仟佰", "仟零");
            yy = outmoney.length;
            if (yy == lett) {
                break
            }
        }
        yy = outmoney.length;
        if (outmoney.charAt(yy - 1) == "零") {
            outmoney = outmoney.substring(0, yy - 1)
        }
        yy = outmoney.length;
        if (outmoney.charAt(yy - 1) == "元") {
            outmoney = outmoney + "整"
        }
        return outmoney
    },
    //支付宝支付
    alipay(){
        var that=this;
        wx.scanCode({
            success(res) {
                wx.showLoading({
                    title: '正在收款中',
                  })
                var value = res.result;
                wx.cloud.callFunction({
                    name: 'pay',
                    data: {
                        "pay_lx": "1",
                        "money":that.data.paynum,
                        "code":value
                    },
                    complete: res => {
                        wx.hideLoading();
                        console.log(res);
                        if(res.result.data.error == '0'){
                            wx.showModal({
                                content: '支付成功,您已成功收款'+that.data.paynum+"元!",
                                showCancel:false,
                                success:function(){
                                    that.hideModal();
                                    that.setData({
                                        paynum:''
                                    })
                                }
                            })
                        }else{
                            wx.showModal({
                              content: '支付失败,请重试!',
                              showCancel:false,
                            })
                        }
                    }
                })
            }
        });
    },
    //微信支付
    wechat(){
        var that=this;
        wx.scanCode({
            success(res) {
                wx.showLoading({
                    title: '正在收款中',
                  })
                var value = res.result;
                wx.cloud.callFunction({
                    name: 'pay',
                    data: {
                        "pay_lx": "2",
                        "money":that.data.paynum,
                        "code":value
                    },
                    complete: res => {
                        wx.hideLoading();
                        if(res.result.data.error == '0'){
                            wx.showModal({
                                content: '支付成功,您已成功收款'+that.data.paynum+"元!",
                                showCancel:false,
                                success:function(){
                                    that.hideModal();
                                    that.setData({
                                        paynum:''
                                    })
                                }
                            })
                        }else{
                            wx.showModal({
                              content: '支付失败,请重试!',
                              showCancel:false,
                            })
                        }
                    }
                })
            }
        });
    },
    hideModal(e) {
        this.setData({
          modalName: null,
        })
      },
    gopay(){
        this.setData({
            modalName: 'Modal',
        })
    }
})