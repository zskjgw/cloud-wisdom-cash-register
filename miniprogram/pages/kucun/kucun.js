/**
 * 库存预警页
 * 
 * 设计：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp();
Page({
  getkc(){
    var that = this;
    wx.cloud.callFunction({
      name: 'kucun',
      data: {
          kucun: true,
          user_id: app.globalData.user_id,
      },
      complete: res => {
        console.log(res);
        if(res.result.error != '1'){
          if(res.result.list.length > 0){
            if(res.result.list[0].goods.length >0){
              that.setData({
                goods:res.result.list[0].goods,
              })
            }else{
              that.setData({
                yd:'',
              })
            }
          }
          else{
            that.setData({
              yd:'',
            })
          }
        }else{
          that.setData({
            yd:'',
          })
        }
      }
    })
  },
  onLoad:function(){
    var that=this;
    that.setData({
      typeArray:null,
      yd:'display:none;',
    })
    this.getkc();
  }
})