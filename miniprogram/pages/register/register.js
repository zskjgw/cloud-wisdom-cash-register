/**
 * 注册页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const db = wx.cloud.database();
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: ['广东省', '广州市', '海珠区'],
  },
  // 地址选择
  RegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  // 注册
  register(e) {
    let data = e.detail.value;
    console.log(data);
    let judge = true;
    for (let i in data) {
      if (i != 'address1') {
        data[i] = data[i].replace(/\s+/g, '')
        if (data[i] === "") {
          judge = false;
        }
      }
    }
    if (judge == false) {
      wx.showModal({
        title: '提示',
        content: '请检查是否留空',
        showCancel: false
      })
    } else {
      wx.showLoading({
        title: '注册账号中',
      })
      wx.cloud.callFunction({
        name: 'sings',
        data: data,
        complete: res => {
          wx.hideLoading();
          wx.showModal({
            title: '提示',
            content: '注册成功',
            showCancel: false,
            success(e) {
              wx.redirectTo({
                url: '../sign/sign',
              });
            }
          })
        }
      })
    }
  }
})