"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// pages/goods/goods.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 点击编辑判断位
    editnum: 0,
    // 编辑-取消文字切换
    edit: '编辑',
    // 复选框选择个数判断
    checkedunm: 0,
    //判断复选框是否全选
    goodschecked: false,
    //判断全选复选框是否全选
    checked: false,
    // 想要删除货品的索引
    goodsIndex: [],
    // 总金额
    addPrice: 0,
    // 货品信息
    goods: [{
      id: 1,
      goods_title: "10克浪味仙蔬菜味10克浪味仙蔬菜味10克浪味仙蔬菜味10克浪味仙蔬菜味10克浪味仙蔬菜味",
      goods_num: 1,
      goods_price: '4.00',
      goods_amount: '4.00'
    }, {
      id: 2,
      goods_title: "aaaaaaaaa",
      goods_num: 1,
      goods_price: '5.00',
      goods_amount: '5.00'
    }, {
      id: 3,
      goods_title: "bbbbbbbbbbbbbb",
      goods_num: 1,
      goods_price: '8.00',
      goods_amount: '8.00'
    }, {
      id: 4,
      goods_title: "ccccccccccccc",
      goods_num: 1,
      goods_price: '7.00',
      goods_amount: '7.00'
    }, {
      id: 5,
      goods_title: "ccccccccccccc",
      goods_num: 1,
      goods_price: '7.00',
      goods_amount: '7.00'
    }]
  },
  onLoad: function onLoad(options) {
    this.addPrice(this);
  },
  // 编辑点击处理
  edit: function edit() {
    var i = this.data.editnum;

    if (i === 0) {
      this.setData({
        editnum: 1,
        edit: '取消',
        checked: false,
        goodschecked: false
      });
    } else {
      this.setData({
        editnum: 0,
        edit: '编辑'
      });
    }
  },
  // 复选框选择
  bindchange: function bindchange(e) {
    this.setData({
      checkedunm: e.detail.value.length,
      goodsIndex: e.detail.value
    });

    if (e.detail.value.length === this.data.goods.length) {
      this.setData({
        checked: true
      });
    } else {
      this.setData({
        checked: false
      });
    }
  },
  // 全选复选框选择
  bindchange_dele: function bindchange_dele(e) {
    if (e.detail.value.length === 1) {
      this.setData({
        goodschecked: true
      });
    } else {
      this.setData({
        goodschecked: false
      });
    }
  },
  // 删除物品
  "delete": function _delete() {
    var that = this; // 判断是否选择了物品

    if (that.data.checkedunm != 0 || that.data.goodschecked === true) {
      wx.showModal({
        title: '提示',
        content: '是否删除',
        success: function success(res) {
          if (res.confirm) {
            // 调用物品删除函数封装
            that.deletePack(that); // 获取总价格

            that.addPrice(that);
          }
        }
      });
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择物品',
        showCancel: false
      });
    }
  },
  // 删除物品函数封装
  deletePack: function deletePack(that) {
    if (that.data.checked === true || that.data.goodschecked === true) {
      that.setData({
        goods: [],
        checked: false,
        goodschecked: false,
        addPrice: 0,
        checkedunm: 0
      });
    } else {
      //获取要删除数组的索引组成数组
      var goodsIndex = that.data.goodsIndex; //数组索引从小到大排序

      goodsIndex.sort(); //获取物品数组

      var Goods_arry = that.data.goods; // 遍历删除

      for (var i = goodsIndex.length - 1; i >= 0; i--) {
        Goods_arry.splice(goodsIndex[i], 1);
      }

      that.setData({
        goods: Goods_arry,
        checked: false,
        goodschecked: false,
        checkedunm: 0
      });
    }
  },
  // 数量加减
  handleItemNumEdit: function handleItemNumEdit(e) {
    // 获取点击单个物品的索引
    var index = e.currentTarget.dataset.index; // 获取单个物品个数

    var num = this.data.goods[index].goods_num; // 获取单个物品价格

    var price = this.data.goods[index].goods_price; // 获取单个物品金额

    var amount = this.data.goods[index].goods_amount; // 对象处理

    var goodsNum = 'goods[' + index + '].goods_num';
    var goodsAmount = 'goods[' + index + '].goods_amount';

    if (num === 1 && e.currentTarget.dataset.operation != '-1') {
      var _this$setData;

      num = num + e.currentTarget.dataset.operation;
      amount = price * num;
      this.setData((_this$setData = {}, _defineProperty(_this$setData, goodsNum, num), _defineProperty(_this$setData, goodsAmount, amount), _this$setData));
    } else if (num > 1) {
      var _this$setData2;

      num = num + e.currentTarget.dataset.operation;
      amount = price * num;
      this.setData((_this$setData2 = {}, _defineProperty(_this$setData2, goodsNum, num), _defineProperty(_this$setData2, goodsAmount, amount), _this$setData2));
    } else {
      wx.showModal({
        title: '提示',
        content: '数额不能小于1',
        showCancel: false
      });
    } // 获取总价格


    this.addPrice(this);
  },
  // 获取总价格封装函数
  addPrice: function addPrice(e) {
    var price = 0;

    for (var i = 0; i < e.data.goods.length; i++) {
      price += parseFloat(e.data.goods[i].goods_amount);
    }

    e.setData({
      addPrice: price
    });
  }
});