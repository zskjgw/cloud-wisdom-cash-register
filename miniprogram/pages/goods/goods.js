/**
 * 快速收银页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // 点击编辑判断位
        editnum: 0,
        // 编辑-取消文字切换
        edit: '编辑',
        // 复选框选择个数判断
        checkedunm: 0,
        //判断复选框是否全选
        goodschecked: false,
        //判断全选复选框是否全选
        checked: false,
        // 想要删除货品的索引
        goodsIndex: [],
        // 总金额
        addPrice: 0,
        price_num:0,
        // 货品信息
        goods: [],
        // 弹框存储
        temporary_goods: [],
        modalName: '',
        // 弹框输入个数
        number: 1
    },
    onLoad: function (options) {
        this.addPrice(this)
    },
    // 扫描条形码
    barcode() {
        var that = this
        wx.scanCode({
            success(res) {
                wx.showLoading({ title: '正在识别中', })
                wx.cloud.callFunction({
                    name: 'commodity',
                    data: {
                        search_goods: true,
                        user_id: app.globalData.user_id,
                        value: res.result,
                    },
                    complete: res => {
                        wx.hideLoading();
                        console.log(res);
                        if(res.result.list.length > 0){
                            let goodslist = res.result.list[0].goods;
                            if (goodslist.length != 0) {
                                let goods = that.data.goods
                                let goodslist = res.result.list[0].goods[0];
                                goodslist.goods_amount2 = goodslist.goods_amount
                                goodslist.goods_num = 1
                                var bzJudge = false;
                                var bzIndex
                                //遍历查看商品是否已经存在
                                for (var i = 0, len = goods.length; i < len; i++) {
                                    if (goods[i].bz === goodslist.bz) {
                                        bzJudge = true;
                                        bzIndex = i;
                                    }
                                }
                                if (bzJudge == true) {
                                    var goodsNum = 'goods[' + bzIndex + '].goods_num';
                                    var goodsAmount = 'goods[' + bzIndex + '].goods_amount2';
                                    var price = goods[bzIndex].goods_amount
                                    let num = parseInt(goods[bzIndex].goods_num + 1)
                                    var amount = price * num
                                    that.setData({
                                        [goodsNum]: num,
                                        [goodsAmount]: amount,
                                    })
                                } else {
                                    goods.push(goodslist);
                                    that.setData({
                                        goods
                                    })
                                }
                                var price_num = that.data.price_num;
                                price_num = parseInt(price_num);
                                price_num = price_num + 1;
                                that.setData({
                                    price_num:price_num
                                })
                                that.addPrice(that);
                            } else {
                                wx.hideLoading();
                                wx.showModal({
                                    title: '提示',
                                    content: '没有该商品',
                                    showCancel: false
                                })
                            }
                        }else {
                            wx.showModal({
                                title: '提示',
                                content: '没有该商品',
                                showCancel: false
                            })
                        }
                    }
                })
            }
        })
    },
    // 搜索
    search_goods(e) {
        var that = this;
        var value = e.detail.value;
        if(value == ''){
            wx.showModal({
              content: '请输入需要搜索的内容',
              showCancel:false,
            })
            return false;
        }
        wx.showLoading({ title: '正在搜索中', })
        wx.cloud.callFunction({
            name: 'commodity',
            data: {
                search_goods: true,
                user_id: app.globalData.user_id,
                value: value,
            },
            complete: res => {
                wx.hideLoading();
                if(res.result.list.length > 0){
                    let goodslist = res.result.list[0].goods
                    if (goodslist.length != 0) {
                        that.setData({
                            temporary_goods: goodslist,
                            modalName: 'Modal'
                        })
                    } else {
                        wx.showModal({
                            title: '提示',
                            content: '没有该商品',
                            showCancel: false
                        })
                    }
                }
                else {
                    wx.showModal({
                        title: '提示',
                        content: '没有该商品',
                        showCancel: false
                    })
                }

            }
        })
    },
    //获取搜索框里的数量
    number(e) {
        this.setData({
            number: e.detail.value
        })
    },
    goods_add(e) {
        var that=this;
        let index = e.currentTarget.dataset.index;
        let number = parseInt(this.data.number);
        let goods = this.data.goods;
        let temporary_goods = this.data.temporary_goods[index];
        var bzJudge = false;
        temporary_goods.goods_amount2 = temporary_goods.goods_amount
        //定位商品位置
        var bzIndex
        // 遍历查看商品是否已经存在
        for (var i = 0, len = goods.length; i < len; i++) {
            if (goods[i].bz === temporary_goods.bz) {
                bzJudge = true;
                bzIndex = i;
            }
        }
        if (bzJudge == true) {
            var goodsNum = 'goods[' + bzIndex + '].goods_num';
            var goodsAmount = 'goods[' + index + '].goods_amount2';
            let num = parseInt(goods[bzIndex].goods_num + number)
            var price = temporary_goods.goods_amount
            var amount = price * num
            this.setData({
                [goodsNum]: num,
                [goodsAmount]: amount,
                temporary_goods: [],
                modalName: '',
                number: 1
            })
        } else {
            temporary_goods.goods_num = number
            goods.push(temporary_goods)
            this.setData({
                goods,
                temporary_goods: [],
                modalName: '',
                number: 1
            })
        }
        var price_num = this.data.price_num;
        price_num = parseInt(price_num);
        price_num++;
        this.setData({
            price_num:price_num
        })
        that.addPrice(that);
    },
    // 关闭弹框
    hideModal(e) {
        this.setData({
            temporary_goods: [],
            modalName: '',
            number: 1
        })
    },
    // 编辑点击处理
    edit() {
        let i = this.data.editnum;
        if (i === 0) {
            this.setData({
                editnum: 1,
                edit: '取消',
                checked: false,
                goodschecked: false
            })
        } else {
            this.setData({
                editnum: 0,
                edit: '编辑'
            })
        }
    },
    // 复选框选择
    bindchange(e) {
        this.setData({
            checkedunm: e.detail.value.length,
            goodsIndex: e.detail.value
        })
        if (e.detail.value.length === this.data.goods.length) {
            this.setData({
                checked: true
            })
        } else {
            this.data.goodschecked = false;
            this.setData({
                checked: false,
            })
        }
    },
    // 全选复选框选择
    bindchange_dele(e) {
        if (e.detail.value.length === 1) {
            this.setData({
                goodschecked: true,
                checkedunm: this.data.goods.length,
            })
        } else {
            this.setData({
                checkedunm: 0,
                goodschecked: false,
                checked: false
            })
        }
    },
    // 删除物品
    delete() {
        let that = this;
        // 判断是否选择了物品
        if (that.data.checkedunm > 0 || that.data.goodschecked === true) {
            wx.showModal({
                title: '提示',
                content: '是否删除',
                success(res) {
                    if (res.confirm) {
                        // 调用物品删除函数封装
                        that.deletePack(that);
                        // 获取总价格
                        that.addPrice(that);
                    }
                }
            })
        } else {
            wx.showModal({
                title: '提示',
                content: '请选择物品',
                showCancel: false
            })
        }
    },
    // 删除物品函数封装
    deletePack(that) {
        if (that.data.checked === true || that.data.goodschecked === true) {
            that.setData({
                goods: [],
                checked: false,
                goodschecked: false,
                addPrice: 0,
                checkedunm: 0
            })
        } else {
            //获取要删除数组的索引组成数组
            let goodsIndex = that.data.goodsIndex;
            //数组索引从小到大排序
            goodsIndex.sort();
            //获取物品数组
            let Goods_arry = that.data.goods;
            // 遍历删除
            for (let i = goodsIndex.length - 1; i >= 0; i--) {
                Goods_arry.splice(goodsIndex[i], 1)
            }
            that.setData({
                goods: Goods_arry,
                checked: false,
                goodschecked: false,
                checkedunm: 0
            })
        }
    },
    // 数量加减
    handleItemNumEdit(e) {
        // 获取点击单个物品的索引
        let index = e.currentTarget.dataset.index;
        // 获取单个物品个数
        let num = this.data.goods[index].goods_num;
        // 获取单个物品价格
        let price = this.data.goods[index].goods_amount;
        // 获取单个物品金额
        let amount = this.data.goods[index].goods_amount2;
        // 对象处理
        var goodsNum = 'goods[' + index + '].goods_num';
        var goodsAmount = 'goods[' + index + '].goods_amount2';
        if (num === 1 && e.currentTarget.dataset.operation != '-1') {
            num = num + e.currentTarget.dataset.operation
            amount = price * num
            this.setData({
                [goodsNum]: num,
                [goodsAmount]: amount
            })
            var price_num = this.data.price_num;
            price_num = parseInt(price_num);
            price_num = price_num + e.currentTarget.dataset.operation;
            this.setData({
                price_num:price_num
            })
        } else if (num > 1) {
            num = num + e.currentTarget.dataset.operation
            amount = price * num
            this.setData({
                [goodsNum]: num,
                [goodsAmount]: amount
            })
            var price_num = this.data.price_num;
            price_num = parseInt(price_num);
            price_num = price_num + e.currentTarget.dataset.operation;
            this.setData({
                price_num:price_num
            })
        } else {
            wx.showModal({
                title: '提示',
                content: '数额不能小于1',
                showCancel: false
            })
        }
        // 获取总价格
        this.addPrice(this);
    },
    // 获取总价格封装函数
    addPrice(e) {
        var price = 0;
        for (let i = 0; i < e.data.goods.length; i++) {
            price += parseFloat(e.data.goods[i].goods_amount2);
        }
        e.setData({
            addPrice: app.gsh_money(price)
        })
    },
    //去结算
    pay_card(e){
        var that=this;
        if(that.data.goods.length > 0){
            app.globalData.pay_goods = that.data;
            wx.navigateTo({
              url: '/pages/collections/collections',
            })
        }else{
            wx.showModal({
              content:'请先添加相关商品',
              showCancel:false,
            })
        }
    },
    onShow(){
        app.globalData.pay_goods = [];
    }
})