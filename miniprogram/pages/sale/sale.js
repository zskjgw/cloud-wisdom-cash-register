/**
 * 销售明细页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-15
 */
const app = getApp();
var that, clientHeight, clientWidth;
Page({
    data: {
        winHeight: "", //窗口高度
        currentTab: 0, //预设当前项的值
        scrollLeft: 0, //tab标题的滚动条位置
        chartTitle: '总订单',
        isMainChartDisplay: true,
        sale: []
    },
    // 滚动切换标签样式
    switchTab: function(e) {
        this.setData({
            currentTab: e.detail.current
        });
        this.checkCor();
    },
    // 点击标题切换当前页时改变样式
    swichNav: function(e) {
        var cur = e.target.dataset.current;
        console.log(cur);
        if (this.data.currentTaB == cur) { return false; } else {
            this.setData({
                currentTab: cur
            });
            // 获取订单信息
            this.sales();
        }
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor: function() {
        if (this.data.currentTab > 4) {
            this.setData({
                scrollLeft: 300
            })
        } else {
            this.setData({
                scrollLeft: 0
            })
        }
    },
    // 获取订单信息
    sales() {
        wx.showLoading({ title: '加载商品中', })
        this.setData({
            sale: []
        })
        let currentTab = this.data.currentTab
        var that = this;
        let date = new Date();
        // 年
        let years = date.getFullYear();
        //    月
        let month = date.getMonth() + 1;
        //    日
        let day = date.getDate();
        if (this.data.currentTab == 0) {
            day = day
        } else if (this.data.currentTab == 1) {
            day = day - 1
        } else if (this.data.currentTab == 2) {
            day = day - 2
        }
        let strat = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 0 + ':' + 0 + ':' + 0).getTime() / 1000);
        let end = Math.round(new Date(years + '/' + month + '/' + day + ' ' + 23 + ':' + 59 + ':' + 59).getTime() / 1000);
        var time = Math.round(new Date() / 1000);
        wx.cloud.callFunction({
            name: 'sale',
            data: {
                user_id:app.globalData.user_id,
                currentTab: currentTab,
                strat: strat,
                end: end
            },
            complete: res => {
                wx.hideLoading();
                that.setData({
                    sale: res.result.data
                })
            }
        })

    },
    onLoad: function() {
        // 获取订单信息
        this.sales();
        that = this;
        //  高度自适应
        wx.getSystemInfo({
            success: function(res) {
                clientHeight = res.windowHeight;
                clientWidth = res.windowWidth;
                var rpxR = 750 / clientWidth;
                var calc = clientHeight * rpxR - 180;
                // console.log(calc)
                that.setData({
                    winHeight: calc
                });
            }
        });
    },
    footerTap: app.footerTap
})