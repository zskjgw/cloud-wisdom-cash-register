/**
 * 登录页
 * 
 * 设计：宋福勇
 * 
 * 审核优化：周俊伟
 * 
 * 最后修改日期:2020-09-17
 */
const db = wx.cloud.database();
const _ = db.command
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    icon: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNzBweCIgaGVpZ2h0PSI3MHB4IiB2aWV3Qm94PSIwIDAgNzAgNzAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDUyLjUgKDY3NDY5KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5pbWdfbWVyY2hhbnRzPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGcgaWQ9InBhZ2VzIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0i55m75b2VLeacquWhqyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEyMS4wMDAwMDAsIC0zNzUuMDAwMDAwKSI+CiAgICAgICAgICAgIDxnIGlkPSJpbWdfbWVyY2hhbnRzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMjEuMDAwMDAwLCAzNzUuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8ZyBpZD0i5ZWG5oi35YWl6am7Ij4KICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNNTUuMDA5NTcwNSwxMC43Nzc3MTQ1IEM1NS43NDAyOTE5LDEwLjc5MjI0IDU2LjQ0MTkxMjIsMTAuNDkxNTQ1NiA1Ni45MzUzMzk2LDkuOTUyMzg0OTEgQzU3LjQ0NjYwNjMsOS4zOTA2NzMxNyA1Ny43MjIzOTM3LDguNjUzOTI2NzcgNTcuNzA1NjQ3Miw3Ljg5NDU2MzAzIEM1Ny43MjUyNDM3LDcuMTMxNDI2MjcgNTcuNDQ5MjYzNSw2LjM5MDIyMjMxIDU2LjkzNTMzOTYsNS44MjU3MzY3OSBDNTYuNDQxMDY5Nyw1LjI4Nzc2NDIzIDU1Ljc0MDAxMjYsNC45ODczMTExNCA1NS4wMDk1NzA1LDUuMDAwNDA3MTQgTDE1LjAwMTU5NSw1LjAwMDQwNzE0IEMxNC4yNzExNTMsNC45ODczMTExNCAxMy41NzAwOTU4LDUuMjg3NzY0MjMgMTMuMDc1ODI1OSw1LjgyNTczNjc5IEMxMi41NjE5MDIsNi4zOTAyMjIzMSAxMi4yODU5MjE4LDcuMTMxNDI2MjcgMTIuMzA1NTE4Myw3Ljg5NDU2MzAzIEMxMi4yNjY1NjI4LDguNjM4MTUwNiAxMi41MzE5MDIyLDkuMzY1NTcxMTEgMTMuMDQwNDc4OCw5LjkwOTQzNjczIEMxMy41NDkwNTU1LDEwLjQ1MzMwMjQgMTQuMjU3MDY4MiwxMC43NjY3NzI3IDE1LjAwMTU5NSwxMC43Nzc3MTQ1IEw1NS4wMDk1NzA1LDEwLjc3NzcxNDUgWiIgaWQ9Iui3r+W+hCIgZmlsbC1vcGFjaXR5PSIwLjQiIGZpbGw9IiMyMzdDRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PC9wYXRoPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNi44MzUzMjQzLDMwLjkxNTc1NzMgQzE4LjkyNjE1OTQsMzAuMzEwNTE1NiAyMC4yMjQ2NzgsMzYuOTc5MTc5IDI0LjAyMTE5NDMsMzcuMDM0MjAwOSBDMjcuODE3NzEwNiwzNy4wODkyMjI4IDI5LjgzMTUxNDksMzUuMDc1NDE4NiAzMi4wNTQ0MDI2LDMxLjA2OTgxODggQzMzLjQ0MDk1NjMsMjkuNTczMjIxMSAzNC4wOTAyMTU3LDMyLjI1ODI5MzUgMzQuNjczNDQ4NiwzMy40MDI3NTA2IEMzNS4yNTY2ODE0LDM0LjU0NzIwNzcgMzcuMDk0NDE1NSwzNy41ODQ0MjA2IDQwLjY1OTgzOTQsMzcuNDMwMzU5MSBDNDQuOTUxNTUzNSwzNy4yNTQyODg4IDQ1LjQ1Nzc1NTYsMzQuODMzMzIxOSA0Ny45NTU3NTMzLDMwLjgyNzcyMjEgQzQ5LjE0NDIyOCwzMC4xMTI0MzY1IDQ5LjY1MDQzMDEsMzEuNDIxOTU5NCA1MC4zODc3MjQ2LDMzLjA5NDYyNzUgQzU0LjMzMjI1NDIsMzEuNjczOTY3MiA1OC42ODgyOTQxLDMxLjk0NDcyNjYgNjIuNDI2NTMyNiwzMy44NDI5MjY0IEM2My40NDg5OTA4LDMyLjI0NDkyNjEgNjQuMDAxODgwNywzMC4zOTE3OTE3IDY0LjAyMjE2OTksMjguNDk0NzkwNCBMNTguNDQyOTQxNywxMy4zNTI3NDMgTDU2LjAyMTk3NDgsMTMuMzUyNzQzIEw1Ni40NTExNDYyLDEzLjgxNDkyNzUgTDU2LjAyMTk3NDgsMTMuMzUyNzQzIEwxMS40MDMxNTc5LDEzLjM1Mjc0MyBMNiwyOC40NzI3ODE2IEM2LDMyLjk5NTU4NzkgOS4yODU4NzI0OSwzNS45NTAxMDg0IDEyLjE1MjM4NDUsMzUuOTUwMTA4NCBDMTUuMDE4ODk2NCwzNS45NTAxMDg0IDE1LjE2MjY1NjMsMzEuNDQzOTY4MyAxNi44MzUzMjQzLDMwLjkxNTc1NzMgWiIgaWQ9Iui3r+W+hCIgZmlsbC1vcGFjaXR5PSIwLjQiIGZpbGw9IiMyMzdDRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PC9wYXRoPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik00OC43MTUwNTY1LDYxLjYxODAxOTIgQzQ4LjI0NzQyNzEsNjAuOTY5MjQ0MiA0Ny42NDE5NSw1OS45OTk5MzA3IDQ2Ljk0NDk1ODcsNTkuMzcwNDcxNCBDNDIuMzE1MjM1Myw1NS4xODkzMjU2IDM5LjE0NTg5NDksNDkuMjE2NDM0NiA0MS4xMDAwMTUyLDQyLjg0NDUyMTQgTDQwLjM5NTczMzksNDIuODQ0NTIxNCBDMzYuODY0MTIyMyw0Mi43ODU2Mjg1IDM0Ljc0MDU0MzQsNDAuNTM5NDU5MyAzMy41NjA2MDQ4LDM3LjIxMDI3MTIgQzMyLjM0NDQ5MTYsNDAuNTQ4ODgxNSAyOS4xOTAzNDQ1LDQyLjc4NzQ0OTggMjUuNjM3NDQwNCw0Mi44MzM1MTcgQzIyLjEwODY2MTIsNDIuNzc2MzExNiAxNy45ODU5MTA3LDQwLjUzNTEyNDEgMTYuODAyMzExMSwzNy4yMTAyNzEyIEMxNS4zMTU4NDgyLDQxLjQ1MjQ5NjMgMTQuMTI4NTQwOCw0Mi40NDgzNjMyIDExLjQ0NzE3NTUsNDIuNDQ4MzYzMiBMMTEuNDQ3MTc1NSw2MS4xMjI4MjE1IEMxMS40NDcxNzU1LDYyLjg1MDUxMTUgMTIuMjUxODk2Myw2NC45NjMzNTUzIDEzLjg2OTU0MjMsNjQuOTYzMzU1MyBMNTAuNjczODM4OCw2NC45NjMzNTUzIEM1MC41MTE5NjYsNjQuNjU3Mzk5MyA1MC4zNzkzMjUzLDY0LjMzNjg1MSA1MC4yNzc2ODA2LDY0LjAwNTk3MyBDNDkuOTgwNTYyLDYzLjQ5OTc3MDggNDkuNDE5MzM3OCw2Mi41NjQzOTcyIDQ4LjcxNTA1NjUsNjEuNjA3MDE0OSBMNDguNzE1MDU2NSw2MS42MTgwMTkyIFoiIGlkPSLot6/lvoQiIGZpbGwtb3BhY2l0eT0iMC40IiBmaWxsPSIjMjM3Q0ZGIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTEwLjAwMTU5NSwxMC43Nzc3MTQ1IEw1NS4wMDk1NzA1LDEwLjc3NzcxNDUgQzU1Ljc0MDI5MTksMTAuNzkyMjQgNTYuNDQxOTEyMiwxMC40OTE1NDU2IDU2LjkzNTMzOTYsOS45NTIzODQ5MSBDNTcuNDQ2NjA2Myw5LjM5MDY3MzE3IDU3LjcyMjM5MzcsOC42NTM5MjY3NyA1Ny43MDU2NDcyLDcuODk0NTYzMDMgQzU3LjcyNTI0MzcsNy4xMzE0MjYyNyA1Ny40NDkyNjM1LDYuMzkwMjIyMzEgNTYuOTM1MzM5Niw1LjgyNTczNjc5IEM1Ni40NDEwNjk3LDUuMjg3NzY0MjMgNTUuNzQwMDEyNiw0Ljk4NzMxMTE0IDU1LjAwOTU3MDUsNS4wMDA0MDcxNCBMMTAuMDAxNTk1LDUuMDAwNDA3MTQgQzkuMjcxMTUyOTYsNC45ODczMTExNCA4LjU3MDA5NTc4LDUuMjg3NzY0MjMgOC4wNzU4MjU5NSw1LjgyNTczNjc5IEM3LjU2MTkwMTk5LDYuMzkwMjIyMzEgNy4yODU5MjE3OSw3LjEzMTQyNjI3IDcuMzA1NTE4Myw3Ljg5NDU2MzAzIEM3LjI2NjU2MjgyLDguNjM4MTUwNiA3LjUzMTkwMjIyLDkuMzY1NTcxMTEgOC4wNDA0Nzg4NSw5LjkwOTQzNjczIEM4LjU0OTA1NTQ4LDEwLjQ1MzMwMjQgOS4yNTcwNjgxNywxMC43NjY3NzI3IDEwLjAwMTU5NSwxMC43Nzc3MTQ1IFoiIGlkPSLot6/lvoQiIHN0cm9rZT0iIzIzN0NGRiIgc3Ryb2tlLXdpZHRoPSIyIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTE2LjgzNTMyNDMsMzAuOTE1NzU3MyBDMTguOTI2MTU5NCwzMC4zMTA1MTU2IDIwLjIyNDY3OCwzNi45NzkxNzkgMjQuMDIxMTk0MywzNy4wMzQyMDA5IEMyNy44MTc3MTA2LDM3LjA4OTIyMjggMjkuODMxNTE0OSwzNS4wNzU0MTg2IDMyLjA1NDQwMjYsMzEuMDY5ODE4OCBDMzMuNDQwOTU2MywyOS41NzMyMjExIDM0LjA5MDIxNTcsMzIuMjU4MjkzNSAzNC42NzM0NDg2LDMzLjQwMjc1MDYgQzM1LjI1NjY4MTQsMzQuNTQ3MjA3NyAzNy4wOTQ0MTU1LDM3LjU4NDQyMDYgNDAuNjU5ODM5NCwzNy40MzAzNTkxIEM0NC45NTE1NTM1LDM3LjI1NDI4ODggNDUuNDU3NzU1NiwzNC44MzMzMjE5IDQ3Ljk1NTc1MzMsMzAuODI3NzIyMSBDNDkuMTQ0MjI4LDMwLjExMjQzNjUgNDkuNjUwNDMwMSwzMS40MjE5NTk0IDUwLjM4NzcyNDYsMzMuMDk0NjI3NSBDNTQuMzMyMjU0MiwzMS42NzM5NjcyIDU4LjY4ODI5NDEsMzEuOTQ0NzI2NiA2Mi40MjY1MzI2LDMzLjg0MjkyNjQgQzYzLjQ0ODk5MDgsMzIuMjQ0OTI2MSA2NC4wMDE4ODA3LDMwLjM5MTc5MTcgNjQuMDIyMTY5OSwyOC40OTQ3OTA0IEw1OC40NDI5NDE3LDEzLjM1Mjc0MyBMNi40MDMxNTc5MywxMy4zNTI3NDMgTDEsMjguNDcyNzgxNiBDMSwzMi45OTU1ODc5IDQuNDk5Mzk3NjEsMzcuMzk3MzQ1OSA4LjgyNDEyNDgxLDM3LjM5NzM0NTkgQzEzLjE0ODg1MiwzNy4zOTczNDU5IDE1LjE2MjY1NjMsMzEuNDQzOTY4MyAxNi44MzUzMjQzLDMwLjkxNTc1NzMgWiIgaWQ9Iui3r+W+hCIgc3Ryb2tlPSIjMjM3Q0ZGIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCI+PC9wYXRoPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik00Ny4xMDg0MTQ5LDU5Ljg0NjMxMTYgQzQxLjU4NDU3NDksNTYuMTE3MDk1MSAzOS4xNDU4OTQ5LDQ5LjIxNjQzNDYgNDEuMTAwMDE1Miw0Mi44NDQ1MjE0IEw0MC4zOTU3MzM5LDQyLjg0NDUyMTQgQzM2Ljg2NDEyMjMsNDIuNzg1NjI4NSAzMy43NDA1NDM0LDQwLjUzOTQ1OTMgMzIuNTYwNjA0OCwzNy4yMTAyNzEyIEMzMS4zNDQ0OTE2LDQwLjU0ODg4MTUgMjguMTkwMzQ0NSw0Mi43ODc0NDk4IDI0LjYzNzQ0MDQsNDIuODMzNTE3IEMyMS4xMDg2NjEyLDQyLjc3NjMxMTYgMTcuOTg1OTEwNyw0MC41MzUxMjQxIDE2LjgwMjMxMTEsMzcuMjEwMjcxMiBDMTUuMzE1ODQ4Miw0MS40NTI0OTYzIDEwLjc0NTI5OTUsNDMuNzY0NDg0OCA2LjQ0NzE3NTUsNDIuNDQ4MzYzMiBMNi40NDcxNzU1LDYxLjEyMjgyMTUgQzYuNDQ3MTc1NSw2Mi44NTA1MTE1IDguMjUxODk2Myw2NC45NjMzNTUzIDkuODY5NTQyMzMsNjQuOTYzMzU1MyBMNTAuNjczODM4OCw2NC45NjMzNTUzIEM1MC41NDUyMTU2LDY0LjYyMTAzNTcgNTAuNDYzMjc4LDY0LjMyMjE3NiA1MC4yNzc2ODA2LDY0LjAwNTk3MyBDNDkuOTgwNTYyLDYzLjQ5OTc3MDggNDkuMzEyMDI2Miw2Mi4zNDA5NjcxIDQ4LjcxNTA1NjUsNjEuNjA3MDE0OSBDNDguNTQ2MjIwNSw2MS4zOTk0MzcyIDQ3LjkzOTgwNCw2MC40MDc1OTMzIDQ3LjEwODQxNDksNTkuODQ2MzExNiBaIiBpZD0i6Lev5b6EIiBzdHJva2U9IiMyMzdDRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTU2LjMyMTkxNCwzNSBDNTAuODcyODk2MywzNS4wMDY2MjA3IDQ2LjA3MzcxNSwzOC41ODc1NTE2IDQ0LjUxNTg2NjIsNDMuODA5MTM2MSBDNDIuOTU4MDE3Myw0OS4wMzA3MjA2IDQ1LjAxMDU4NzYsNTQuNjU1ODU3MyA0OS41NjUyMTU1LDU3LjY0NzA0NDggQzUwLjM5MTQ5NjIsNTguMzQ0MTI4OCA1MS4xMzA0ODI3LDU5LjEzODUzOTMgNTEuNzY2MDk0NSw2MC4wMTI5ODk3IEM1Mi40NDg1NjU1LDYwLjk2NTc4OTUgNTMuMDgwMzYsNjEuOTUzODg2NiA1My42NTg4NTA0LDYyLjk3MzE3MiBDNTMuOTc0OTQzMSw2NC4yMDgxODk3IDU1LjEwMTAxMTIsNjUuMDYyMDYzMSA1Ni4zNzU1MTAyLDY1LjAzMzE1NjkgQzU3LjY1MDAwOTMsNjUuMDA0MjUwOCA1OC43MzYyMDcsNjQuMTAwMjAyMyA1OC45OTU5ODIsNjIuODUyMTIzNiBDNTkuMzU2MDE3NCw2MS43NTY3MDIxIDU5Ljk0MDcxMTUsNjAuNzQ4NDc5NiA2MC43MTI2Njc2LDU5Ljg5MTk0MTQgQzYxLjUyNjU5NTgsNTguOTk5MzE2MyA2Mi40MjU5MDM2LDU4LjE4ODQ2NSA2My4zOTc3NCw1Ny40NzA5NzQ1IEw2My44MDQ5MDI2LDU3LjE3Mzg1NTkgTDY0LjE1NzA0MzIsNTYuOTQyNzYzNSBMNjQuMDU4MDAzNyw1Ni45NDI3NjM1IEw2NC4xNjgwNDc2LDU2Ljg1NDcyODQgQzY4LjE3NzUxNzksNTMuNTQwMjEzIDY5LjY3NjgwMDIsNDguMDY2NjgyIDY3LjkxNjA1Nyw0My4xNzE2MTg1IEM2Ni4xNTUzMTM4LDM4LjI3NjU1NSA2MS41MTMwMTEzLDM1LjAxMjEzODEgNTYuMzEwOTA5NiwzNS4wMTEwMDQ0IEw1Ni4zMjE5MTQsMzUgWiBNNTYuMzIxOTE0LDUzLjA2OTIxNjUgQzUzLjE0OTQyOCw1My4wNjkyMTY1IDUwLjU3NzYxOTksNTAuNDk3NDA4NCA1MC41Nzc2MTk5LDQ3LjMyNDkyMjQgQzUwLjU3NzYxOTksNDQuMTUyNDM2MyA1My4xNDk0Mjc5LDQxLjU4MDYyODIgNTYuMzIxOTE0LDQxLjU4MDYyODIgQzU5LjQ5NDQsNDEuNTgwNjI4MiA2Mi4wNjYyMDgxLDQ0LjE1MjQzNjMgNjIuMDY2MjA4Miw0Ny4zMjQ5MjIzIEM2Mi4wNjYyMDgyLDQ4Ljg0ODQwMzkgNjEuNDYxMDA3NSw1MC4zMDk0ODc2IDYwLjM4Mzc0MzQsNTEuMzg2NzUxNyBDNTkuMzA2NDc5Myw1Mi40NjQwMTU4IDU3Ljg0NTM5NTYsNTMuMDY5MjE2NSA1Ni4zMjE5MTQsNTMuMDY5MjE2NSBaIiBpZD0i5b2i54q2IiBmaWxsPSIjRkY5QTJGIiBmaWxsLXJ1bGU9Im5vbnplcm8iPjwvcGF0aD4KICAgICAgICAgICAgICAgICAgICA8cmVjdCBpZD0i55+p5b2iIiBmaWxsLXJ1bGU9Im5vbnplcm8iIHg9IjAiIHk9IjAiIHdpZHRoPSI3MCIgaGVpZ2h0PSI3MCI+PC9yZWN0PgogICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4='
  },
  //自动登录
  fastlogin(){
    wx.showLoading({
      title: '获取用户信息中',
    })
    wx.cloud.callFunction({
      name: 'login',
      data: {
        fastlogin: true,
      },
      complete: res => {
        wx.hideLoading();
        if(res.result.error == '0'){
          wx.showToast({
            icon:'none',
            title: '登录成功',
          })
          app.globalData.user_id = res.result.user_id;
          app.globalData.address1 = res.result.address1;
          app.globalData.address2 = res.result.address2;
          app.globalData.shop = res.result.shop;
          app.globalData.guest = res.result.guest;
          wx.redirectTo({
            url: '/pages/index/index',
          });
        }else{
          wx.showToast({
            icon:'none',
            title: '请先登录或体验',
          })
        }
      }
    })
  },
  // 登陆点击
  sign(e) {
    let user = e.detail.value.user.replace(/\s+/g, '');
    let password = e.detail.value.password.replace(/\s+/g, '');
    if (user === "") {
      wx.showModal({
        title: '提示',
        content: '请填写用户名',
        showCancel: false
      })
    } else if (password === "") {
      wx.showModal({
        title: '提示',
        content: '请填写密码',
        showCancel: false
      })
    } else {
      wx.cloud.callFunction({
        name: 'login',
        data: {
          user: user,
          password:password,
          guest:0,
        },
        complete: res => {
          console.log(res);
          if(res.result.error == '0'){
            app.globalData.user_id = res.result.user_id;
            app.globalData.address1 = res.result.address1;
            app.globalData.address2 = res.result.address2;
            app.globalData.shop = res.result.shop;
            app.globalData.guest = 0;
            wx.redirectTo({
              url: '/pages/index/index',
            });
          }else{
            wx.showModal({
              content: res.result.msg,
              showCancel:false,
            })
          }
        }
      })
    }
  },
  guest_login(){
    let user = 'xxxxx';
    let password = 'xxxx';
    wx.cloud.callFunction({
      name: 'login',
      data: {
        user: user,
        password:password,
        guest:1,
      },
      complete: res => {
        console.log(res);
        if(res.result.error == '0'){
          app.globalData.user_id = res.result.user_id;
          app.globalData.address1 = res.result.address1;
          app.globalData.address2 = res.result.address2;
          app.globalData.shop = res.result.shop;
          app.globalData.guest = 1;
          wx.redirectTo({
            url: '/pages/index/index',
          });
        }else{
          wx.showModal({
            content: res.result.msg,
            showCancel:false,
          })
        }
      }
    })
  },
  register() {
    wx.navigateTo({
      url: '../register/register',
    });

  },
  onLoad(){
    this.fastlogin();
  }
})