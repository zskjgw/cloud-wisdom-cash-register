### 部署说明
1. 克隆项目到本地

2. 修改 project.config.json 中的 appid 替换为你自己的 appid

3. 使用微信开发者工具，导入项目

4. 创建云数据库集合 Signs、commodity、login_history、pay_count (数据库集合权限为“所有用户可读，仅创建者可读写”）

5. 修改pay内云函数的主动扫码支付接口地址 这里由于我是个人小程序 没有支付权限 采用此方法 需要可以改为云函数支付

6. 上传并部署cloudfunction内的云函数

7. 开始使用