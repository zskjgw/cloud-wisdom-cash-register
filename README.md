#### cloud-wisdom-cash-register

### 项目名称
	云智慧收银

### 项目介绍
该项目是我团队开发的一款智慧云收银小程序。本项目致力打造一个掌上即可轻松完成收银工作的系统，方便各大商超客户使用，目前该版本为1.0体验内测版，后期我们会对接PC端收银系统，打造更完善的功能。

开发时间：2020年9月13日至2020年9月17日 共5天

### 项目效果截图
![首页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E9%A6%96%E9%A1%B5.PNG?sign=58584c181096628e439d2496f6eecede&t=1600320273)

### 首页

![快速收银页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%BF%AB%E9%80%9F%E6%94%B6%E9%93%B6.PNG?sign=001945699ef0ccd1abb73b9c86880645&t=1600320291)

### 快速收银页

![商品管理页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%95%86%E5%93%81%E7%AE%A1%E7%90%86.PNG?sign=89bb3231e4a98081681a786153d574da&t=1600320311)

### 商品管理页

![主动收款页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E4%B8%BB%E5%8A%A8%E6%94%B6%E6%AC%BE.PNG?sign=b9d1b44a0185dcaf79c6415b4f75a88d&t=1600320323)

### 主动收款页

![销售报表统计页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E9%94%80%E5%94%AE%E6%8A%A5%E8%A1%A8.PNG?sign=918c60ee95f02abe667a97ea0ecdbf52&t=1600321121)

### 销售报表统计页

![销售明细页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E9%94%80%E5%94%AE%E6%98%8E%E7%BB%86.PNG?sign=b8efb3415731abb711596070350f7735&t=1600320347)

### 销售明细页

![库存预警页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%BA%93%E5%AD%98%E5%BC%82%E5%8A%A8.PNG?sign=ea0777c90099f03e893dee7369cc538b&t=1600320359)

### 库存预警页

​![盘点系统页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E7%9B%98%E7%82%B9.PNG?sign=2e450643c027beef59638d48a961a744&t=1600320372)

### 盘点系统页

​![结算页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E7%BB%93%E7%AE%97.PNG?sign=4334e702b0aeda57f631f43ad67872ba&t=1600320385)

### 结算页

​![登录页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E7%99%BB%E5%BD%95.PNG?sign=f791e5ebe48346783a9ddde1971291c0&t=1600320887)

### 登录页

![注册页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E6%B3%A8%E5%86%8C.PNG?sign=b33ea4af24a2e64e747e102135032089&t=1600320411)

### 注册页

​![关于我们页](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%85%B3%E4%BA%8E%E6%88%91%E4%BB%AC.PNG?sign=8b4bad0fda73f6950faf15e9d5d190ac&t=1600320422)

### 关于我们页

### 项目小程序线上二维码
![Alt](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%B0%8F%E7%A8%8B%E5%BA%8F%E7%BA%BF%E4%B8%8A%E7%A0%81.jpg?sign=9962ad371eb2157b451bb66532639cae&t=1600320518)

### 项目小程序体验二维码
![Alt](https://6162-abcde-oaf5o-1302086030.tcb.qcloud.la/zhysy/%E5%B0%8F%E7%A8%8B%E5%BA%8F%E4%BD%93%E9%AA%8C%E7%A0%81.png?sign=78366e27d63953f8199cd8042ab56b5a&t=1600320564)


### 部署说明
1. 克隆项目到本地

2. 修改 project.config.json 中的 appid 替换为你自己的 appid

3. 使用微信开发者工具，导入项目

4. 创建云数据库集合 Signs、commodity、login_history、pay_count (数据库集合权限为“所有用户可读，仅创建者可读写”）

5. 修改pay内云函数的主动扫码支付接口地址 这里由于我是个人小程序 没有支付权限 采用此方法 需要可以改为云函数支付

6. 上传并部署cloudfunction内的云函数

7. 开始使用

### 开源许可证标注
	Apache-2.0 LICENSE